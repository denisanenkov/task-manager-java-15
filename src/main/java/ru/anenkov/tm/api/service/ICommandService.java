package ru.anenkov.tm.api.service;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.dto.Command;

import java.util.List;

public interface ICommandService {

    String[] getCommands();

    String[] getArgs();

    List<AbstractCommand> getCommandList();

}