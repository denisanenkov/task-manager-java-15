package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommands();

    List<AbstractCommand> getCommandList();
}
