package ru.anenkov.tm.bootstrap;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.command.*;
import ru.anenkov.tm.command.auth.LoginCommand;
import ru.anenkov.tm.command.auth.LogoutCommand;
import ru.anenkov.tm.command.auth.RegisterCommand;
import ru.anenkov.tm.command.project.*;
import ru.anenkov.tm.command.project.remove.ProjectClearCommand;
import ru.anenkov.tm.command.project.remove.RemoveProjectByIdCommand;
import ru.anenkov.tm.command.project.remove.RemoveProjectByIndexCommand;
import ru.anenkov.tm.command.project.remove.RemoveProjectByNameCommand;
import ru.anenkov.tm.command.project.show.ProjectShowCommand;
import ru.anenkov.tm.command.project.show.ShowProjectByIdCommand;
import ru.anenkov.tm.command.project.show.ShowProjectByIndexCommand;
import ru.anenkov.tm.command.project.show.ShowProjectByNameCommand;
import ru.anenkov.tm.command.project.update.UpdateProjectByIdCommand;
import ru.anenkov.tm.command.project.update.UpdateProjectByIndexCommand;
import ru.anenkov.tm.command.system.*;
import ru.anenkov.tm.command.task.*;
import ru.anenkov.tm.command.task.remove.RemoveTaskByIdCommand;
import ru.anenkov.tm.command.task.remove.RemoveTaskByIndexCommand;
import ru.anenkov.tm.command.task.remove.RemoveTaskByNameCommand;
import ru.anenkov.tm.command.task.remove.TaskClearCommand;
import ru.anenkov.tm.command.task.show.ShowTaskByIdCommand;
import ru.anenkov.tm.command.task.show.ShowTaskByIndexCommand;
import ru.anenkov.tm.command.task.show.ShowTaskByNameCommand;
import ru.anenkov.tm.command.task.show.TaskShowCommand;
import ru.anenkov.tm.command.task.update.UpdateTaskByIdCommand;
import ru.anenkov.tm.command.task.update.UpdateTaskByIndexCommand;
import ru.anenkov.tm.command.user.*;
import ru.anenkov.tm.command.user.update.*;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.repository.CommandRepository;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.*;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    {
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new RegisterCommand());
        registry(new ShowUserProfileCommand());
        registry(new UpdatePasswordCommand());
        registry(new UpdateUserEmailCommand());
        registry(new UpdateUserFirstNameCommand());
        registry(new UpdateUserMiddleNameCommand());
        registry(new UpdateUserLastNameCommand());
        registry(new VersionCommand());
        registry(new AboutCommand());
        registry(new ShowArgumentsCommand());
        registry(new ShowCommandsCommand());
        registry(new TaskShowCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new ShowTaskByIndexCommand());
        registry(new ShowTaskByIdCommand());
        registry(new ShowTaskByNameCommand());
        registry(new UpdateTaskByIndexCommand());
        registry(new UpdateTaskByIdCommand());
        registry(new RemoveTaskByIdCommand());
        registry(new RemoveTaskByIndexCommand());
        registry(new RemoveTaskByNameCommand());
        registry(new ProjectShowCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ShowProjectByIndexCommand());
        registry(new ShowProjectByIdCommand());
        registry(new ShowProjectByNameCommand());
        registry(new UpdateProjectByIndexCommand());
        registry(new UpdateProjectByIdCommand());
        registry(new RemoveProjectByIdCommand());
        registry(new RemoveProjectByIndexCommand());
        registry(new RemoveProjectByNameCommand());
        registry(new ExitCommand());
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private void initUsers() {
        userService.create("1", "1", "test@mail.ru");
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new IncorrectCommandException(String.valueOf(command));
        command.execute();
    }

    public void run(final String[] args) {
        System.out.println(MessageConst.WELCOME);
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        return true;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}