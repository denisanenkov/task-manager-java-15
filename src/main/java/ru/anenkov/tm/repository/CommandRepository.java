package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.command.auth.LoginCommand;
import ru.anenkov.tm.command.auth.LogoutCommand;
import ru.anenkov.tm.command.auth.RegisterCommand;
import ru.anenkov.tm.command.project.ProjectCreateCommand;
import ru.anenkov.tm.command.project.remove.ProjectClearCommand;
import ru.anenkov.tm.command.project.remove.RemoveProjectByIdCommand;
import ru.anenkov.tm.command.project.remove.RemoveProjectByIndexCommand;
import ru.anenkov.tm.command.project.remove.RemoveProjectByNameCommand;
import ru.anenkov.tm.command.project.show.ProjectShowCommand;
import ru.anenkov.tm.command.project.show.ShowProjectByIdCommand;
import ru.anenkov.tm.command.project.show.ShowProjectByIndexCommand;
import ru.anenkov.tm.command.project.show.ShowProjectByNameCommand;
import ru.anenkov.tm.command.project.update.UpdateProjectByIdCommand;
import ru.anenkov.tm.command.project.update.UpdateProjectByIndexCommand;
import ru.anenkov.tm.command.system.*;
import ru.anenkov.tm.command.task.TaskCreateCommand;
import ru.anenkov.tm.command.task.remove.RemoveTaskByIdCommand;
import ru.anenkov.tm.command.task.remove.RemoveTaskByIndexCommand;
import ru.anenkov.tm.command.task.remove.RemoveTaskByNameCommand;
import ru.anenkov.tm.command.task.remove.TaskClearCommand;
import ru.anenkov.tm.command.task.show.ShowTaskByIdCommand;
import ru.anenkov.tm.command.task.show.ShowTaskByIndexCommand;
import ru.anenkov.tm.command.task.show.ShowTaskByNameCommand;
import ru.anenkov.tm.command.task.show.TaskShowCommand;
import ru.anenkov.tm.command.task.update.UpdateTaskByIdCommand;
import ru.anenkov.tm.command.task.update.UpdateTaskByIndexCommand;
import ru.anenkov.tm.command.user.ShowUserProfileCommand;
import ru.anenkov.tm.command.user.update.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new HelpCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new LoginCommand());
        commandList.add(new LogoutCommand());
        commandList.add(new RegisterCommand());
        commandList.add(new ShowUserProfileCommand());
        commandList.add(new UpdatePasswordCommand());
        commandList.add(new UpdateUserEmailCommand());
        commandList.add(new UpdateUserFirstNameCommand());
        commandList.add(new UpdateUserMiddleNameCommand());
        commandList.add(new UpdateUserLastNameCommand());
        commandList.add(new VersionCommand());
        commandList.add(new AboutCommand());
        commandList.add(new ShowArgumentsCommand());
        commandList.add(new ShowCommandsCommand());
        commandList.add(new TaskShowCommand());
        commandList.add(new TaskClearCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new ShowTaskByIndexCommand());
        commandList.add(new ShowTaskByIdCommand());
        commandList.add(new ShowTaskByNameCommand());
        commandList.add(new UpdateTaskByIndexCommand());
        commandList.add(new UpdateTaskByIdCommand());
        commandList.add(new RemoveTaskByIdCommand());
        commandList.add(new RemoveTaskByIndexCommand());
        commandList.add(new RemoveTaskByNameCommand());
        commandList.add(new ProjectShowCommand());
        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectCreateCommand());
        commandList.add(new ShowProjectByIndexCommand());
        commandList.add(new ShowProjectByIdCommand());
        commandList.add(new ShowProjectByNameCommand());
        commandList.add(new UpdateProjectByIndexCommand());
        commandList.add(new UpdateProjectByIdCommand());
        commandList.add(new RemoveProjectByIdCommand());
        commandList.add(new RemoveProjectByIndexCommand());
        commandList.add(new RemoveProjectByNameCommand());
        commandList.add(new ExitCommand());
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return commandList;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}